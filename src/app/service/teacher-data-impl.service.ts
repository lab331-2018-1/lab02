import { Injectable } from '@angular/core';
import { extend } from '../../../node_modules/webdriver-js-extender';
import { TeacherService } from './teacher-service';
import { Teacher } from '../entity/teacher';
import { Observable, of } from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class TeacherDataImplService extends TeacherService {
  constructor() {
    super();
   }
   getTeachers(): Observable<Teacher[]>{
     return of(this.teachers)
   }
   teachers: Teacher[] = [{
    'teacherId' : '007',
    'name' : 'Dto',
    'surname' : 'chatchai'
  },
  {'teacherId' : '009',
    'name' : 'Kong',
    'surname' : 'Konichiwa'
 }
]
}
