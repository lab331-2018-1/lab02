import { Observable } from "rxjs";
import { Teacher } from "../entity/teacher"
export abstract class TeacherService {
    abstract getTeachers(): Observable<Teacher[]>;
}
