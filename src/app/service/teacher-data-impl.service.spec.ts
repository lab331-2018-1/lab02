import { TestBed, inject } from '@angular/core/testing';

import { TeacherDataImplService } from './teacher-data-impl.service';

describe('TeacherDataImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeacherDataImplService]
    });
  });

  it('should be created', inject([TeacherDataImplService], (service: TeacherDataImplService) => {
    expect(service).toBeTruthy();
  }));
});
