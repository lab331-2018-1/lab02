import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { StudentService } from './service/student-service';
import { StudentDataImplService } from './service/student-data-impl.service';
import { TeachersComponent } from './teachers/teachers.component';
import { TeacherService } from './service/teacher-service';
import { TeacherDataImplService } from './service/teacher-data-impl.service';

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    TeachersComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ {provide: StudentService, useClass: StudentDataImplService}
  ,{provide:TeacherService, useClass: TeacherDataImplService}],
  bootstrap: [AppComponent]
})
export class AppModule { }
